from typing import Dict

import numpy as np
import pandas as pd

from utils import invert_dict, PLUS, MINUS


class LiSignature:
    name = "Li et al. 2019"
    genes = invert_dict({
        PLUS: ["LYG1", "MSH4", "RPL22L1"],
        MINUS: [
            "DDX27", "EPM2AIP1", "HENMT1", "MLH1", "NHLRC1", "NOL4L",
            "RNLS", "RTFDC1", "SHROOM4", "SMAP1", "TTC30A", "ZSWIM3",
        ]
    })

    gene_translations = {
        'ENSG00000022277': 'RTFDC1',
        'ENSG00000057468': 'MSH4',
        'ENSG00000076242': 'MLH1',
        'ENSG00000112305': 'SMAP1',
        'ENSG00000124228': 'DDX27',
        'ENSG00000132801': 'ZSWIM3',
        'ENSG00000144214': 'LYG1',
        'ENSG00000158352': 'SHROOM4',
        'ENSG00000162639': 'HENMT1',
        'ENSG00000163584': 'RPL22L1',
        'ENSG00000178567': 'EPM2AIP1',
        'ENSG00000184719': 'RNLS',
        'ENSG00000187566': 'NHLRC1',
        'ENSG00000197183': 'NOL4L',
        'ENSG00000197557': 'TTC30A'
    }

    def get_scores(self, dataset: pd.DataFrame, labels: Dict[str, bool] = None) -> Dict[str, float]:
        dataset = dataset.apply(np.log10)
        dataset = dataset[dataset.index.isin(self.genes.keys())]
        assert set(self.genes.keys()) == set(dataset.index.values)  # all genes are in dataset
        scores: Dict[str, float] = dict()
        for sample_name in dataset:
            scores[sample_name] = sum([
                expression * self.genes[gene]
                for gene, expression in dataset[sample_name].items()
            ])
        return scores
