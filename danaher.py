from typing import Dict

import numpy as np
import pandas as pd

from mmr_hps.hypermutation_predictor import HypermutationPredictorSignature
from mmr_hps.mmr_loss import MMRLossSignature


class DanaherSignature:
    name = "Danaher et al. 2019"

    def get_scores(self, dataset: pd.DataFrame, labels: Dict[str, bool] = None) -> Dict[str, float]:
        hps_scores = HypermutationPredictorSignature().get_scores(dataset, labels)
        mmr_loss_scores = MMRLossSignature().get_scores(dataset, labels)

        danaher_signature_values = dict()
        samples = hps_scores.keys()
        for sample_name in samples:
            danaher_signature_values[sample_name][self.name] = np.float_power(
                np.power(max(hps_scores[sample_name], 0), 2) +
                np.power(min(mmr_loss_scores[sample_name], 0), 2),
                0.5
            )
        return danaher_signature_values
