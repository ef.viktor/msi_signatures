from typing import Dict

import numpy as np
import pandas as pd

from utils import invert_dict, PLUS, MINUS


class PacincovaSignature:
    name = "Pacincova Popovici et al. 2019"
    genes = invert_dict({
        PLUS: ["TNNT1", "VNN2", "TRIM7", "TNFSF9", "AGR2", "RPL22L1"],
        MINUS: [
            "GNG4", "KRT23", "ACSL6", "TNNC2", "SHROOM2", "EPDR1",
            "ARID3A", "KHDRBS3", "GGT7", "SHROOM4", "MLH1", "NKD1",
            "TFCP2L1", "VAV3", "RUBCNL", "ASCL2", "AXIN2", "PLAGL2",
            "PRR15",
        ]
    })

    gene_translations = {
        'ENSG00000076242': 'MLH1',
        'ENSG00000086289': 'EPDR1',
        'ENSG00000101470': 'TNNC2',
        'ENSG00000102445': 'RUBCNL',
        'ENSG00000106541': 'AGR2',
        'ENSG00000108244': 'KRT23',
        'ENSG00000112303': 'VNN2',
        'ENSG00000115112': 'TFCP2L1',
        'ENSG00000116017': 'ARID3A',
        'ENSG00000125657': 'TNFSF9',
        'ENSG00000126003': 'PLAGL2',
        'ENSG00000131067': 'GGT7',
        'ENSG00000131773': 'KHDRBS3',
        'ENSG00000134215': 'VAV3',
        'ENSG00000140807': 'NKD1',
        'ENSG00000146054': 'TRIM7',
        'ENSG00000146950': 'SHROOM2',
        'ENSG00000158352': 'SHROOM4',
        'ENSG00000163584': 'RPL22L1',
        'ENSG00000164398': 'ACSL6',
        'ENSG00000168243': 'GNG4',
        'ENSG00000168646': 'AXIN2',
        'ENSG00000176532': 'PRR15',
        'ENSG00000183734': 'ASCL2',
        'ENSG00000105048': 'TNNT1'
    }

    def get_scores(self, dataset: pd.DataFrame, labels: Dict[str, bool] = None) -> Dict[str, float]:
        dataset = dataset.apply(np.log10)
        dataset = dataset[dataset.index.isin(self.genes.keys())]
        assert set(self.genes.keys()) == set(dataset.index.values)  # all genes are in dataset
        scores: Dict[str, float] = dict()
        for sample_name in dataset:
            scores[sample_name] = sum([
                expression * self.genes[gene]
                for gene, expression in dataset[sample_name].items()
            ])
        return scores
