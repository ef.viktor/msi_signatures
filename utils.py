from collections import defaultdict
from typing import Iterable

import numpy as np
from sklearn.mixture import GaussianMixture

MINUS = -1
PLUS = 1


def mixture_model(values: Iterable, n_components: int):
    two_dimension_values = [(v, 0) for v in values]
    gmm = GaussianMixture(n_components=n_components).fit(two_dimension_values)
    labels = gmm.predict(two_dimension_values)
    groups = defaultdict(list)
    for label, value in zip(labels, values):
        groups[label].append(value)
    return [np.mean(group) for group in groups.values()]


def invert_dict(dictionary: dict):
    ret = dict()
    for key, value in dictionary.items():
        for i in value:
            ret[i] = key
    return ret
