from typing import Dict

import numpy as np
import pandas as pd

from utils import mixture_model


class HypermutationPredictorSignature:
    name = "Danaher et al. 2019: Hypermutation Predictor Score"
    genes = {
        "EPM2AIP1": -0.31218,
        "TTC30A": -0.19894,
        "SMAP1": -0.1835,
        "RNLS": -0.19023,
        "WNT11": -0.11515,
        "SFXN1": 0.214676,
        "SREBF1": 0.194835,
        "TYMS": 0.206972,
        "EIF5AL1": 0.194935,
        "WDR76": 0.188582,
    }
    gene_translations = {
        "ENSG00000178567": "EPM2AIP1",
        "ENSG00000197557": "TTC30A",
        "ENSG00000112305": "SMAP1",
        "ENSG00000184719": "RNLS",
        "ENSG00000085741": "WNT11",
        "ENSG00000164466": "SFXN1",
        "ENSG00000072310": "SREBF1",
        "ENSG00000176890": "TYMS",
        "ENSG00000253626": "EIF5AL1",
        "ENSG00000092470": "WDR76"
    }

    def get_scores(self, dataset: pd.DataFrame, labels: Dict[str, bool] = None) -> Dict[str, float]:
        if labels is None: raise ValueError("Must provide MSI state labels")
        dataset = dataset.apply(np.log2)
        scores: Dict[str, float] = dict()
        for sample_name in dataset:
            scores[sample_name] = sum([
                expression * self.genes[gene]
                for gene, expression in dataset[sample_name].items()
            ])
        # calc mean and sigma on MSS-only samples
        mss_samples = [k for k, v in labels.items() if v is False]
        mss_samples_scores = {k: v for k, v in scores.items() if k in mss_samples}
        mean = min(mixture_model(mss_samples_scores.values(), 2))
        sigma = np.std(list(mss_samples_scores.values()))

        # calc Z-value
        hps_values: Dict[str, float] = dict()
        for sample, score in scores.items():
            # Z=(x-µ)/σ
            hps_values[sample] = (score - mean) / sigma
        return scores
