from typing import Dict

import numpy as np
import pandas as pd

from utils import invert_dict, MINUS, mixture_model


class MMRLossSignature:
    name = "Danaher et al. 2019: MMR Loss Score"
    genes = invert_dict({
        MINUS: ["MLH1", "MSH2", "MSH6", "PMS2"]
    })

    gene_translations = {
        "ENSG00000076242": "MLH1",
        "ENSG00000095002": "MSH2",
        "ENSG00000116062": "MSH6",
        "ENSG00000122512": "PMS2",
    }

    def __init__(self, use_gaussian: bool = False):
        self.use_gaussian = use_gaussian

    def get_scores(self, dataset: pd.DataFrame, labels: Dict[str, bool] = None) -> Dict[str, float]:
        if labels is None: raise ValueError("Must provide MSI state labels")
        dataset = dataset.apply(np.log2)
        # filter genes
        dataset = dataset[dataset.index.isin(self.genes.keys())]
        assert set(self.genes.keys()) == set(dataset.index.values)  # all genes are in dataset
        # sort samples
        mss_cases = [k for k, v in labels.items() if v is False]
        # calc mean and sigma for every gene on MSS cases
        gene_coefficients = dict()
        mss_samples = dataset.filter(mss_cases)
        for gene, expressions in mss_samples.iterrows():
            if self.use_gaussian:
                mean = max(mixture_model(expressions, 2))
            else:
                mean = np.median(expressions)
            sigma = np.std(expressions)
            gene_coefficients[gene] = dict(
                mean=mean,
                sigma=sigma
            )
        # calc MLS for all cases
        mls_values: Dict[str, float] = dict()
        for sample_name in dataset:
            # Z = (x-µ)/σ
            z_min = min([
                (expression - gene_coefficients[gene]["mean"]) / gene_coefficients[gene]["sigma"]
                for gene, expression in dataset[sample_name].items()
            ])
            # 1.03 and 0.69 are the theoretical expectation and SD of the minimum of 4 standard normal random variables
            mls_values[sample_name] = (z_min + 1.03) / 0.69
        return mls_values
